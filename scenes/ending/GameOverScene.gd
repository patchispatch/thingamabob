extends Node

@onready var gameovercutscene_animation_player := $GameOverCutscene/AnimationPlayer
@onready var animation_player := $AnimationPlayer
@onready var black_rect := $BlackRect

# Called when the node enters the scene tree for the first time.
func _ready():
	animate_specific_game_over(Global.game_over_reason)

func _input(event):
	if event.is_action_pressed("GameStart"):
		# Changes the scene
		transition_into_new_game()

func animate_specific_game_over(reason: String):
	if (reason == "crash"):
		gameovercutscene_animation_player.play("crash")
	else:
		gameovercutscene_animation_player.play("bonsaideath")

func transition_into_new_game():
	black_rect.visible = true
	animation_player.play("fade")
	await animation_player.animation_finished
	get_tree().change_scene_to_file("res://scenes/main.tscn")
