extends Node

@onready var victory_cutscene := $VictoryCutscene
@onready var victory_cutscene_animation := $VictoryCutscene/AnimationPlayer
@onready var left_key_animation := $LeftKey/AnimationPlayer
@onready var right_key_animation := $RightKey/AnimationPlayer
@onready var again_key_animation := $AgainKey/AnimationPlayer

@onready var animation_player := $AnimationPlayer
@onready var black_rect := $BlackRect

# Called when the node enters the scene tree for the first time.
func _ready():
	left_key_animation.play("Invisible")
	right_key_animation.play("Visible")
	again_key_animation.play("Invisible")
	victory_cutscene_animation.play("1")


func _input(event):
	if event.is_action_pressed("CarLeft"):
		animate_previous_page()
	if event.is_action_pressed("CarRight"):
		animate_next_page()
		
	if event.is_action_pressed("GameStart"):
		# Changes the scene
		transition_into_new_game()



func animate_previous_page() -> void:
	var current = victory_cutscene_animation.current_animation
	if (int(current) > 1):
		var prev = int(current) - 1
		victory_cutscene_animation.play(str(prev))
		right_key_animation.play("Visible")
		again_key_animation.play("Invisible")
		if (prev == 1):
			left_key_animation.play("Invisible")
	

func animate_next_page() -> void:
	var current = victory_cutscene_animation.current_animation
	if (int(current) < 6):
		var next = int(current) + 1
		victory_cutscene_animation.play(str(next))
		left_key_animation.play("Visible")
		if (next == 6):
			right_key_animation.play("Invisible")
			again_key_animation.play("Visible")


func transition_into_new_game():
	var current = victory_cutscene_animation.current_animation
	if (int(current) == 6):
		black_rect.visible = true
		animation_player.play("fade")
		await animation_player.animation_finished
		get_tree().change_scene_to_file("res://scenes/main.tscn")
