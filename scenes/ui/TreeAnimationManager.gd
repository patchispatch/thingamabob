extends Node
class_name TreeAnimationManager

@onready var tree_animation := $Bonsai/AnimationPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	tree_animation.play("Sano1")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func play_current_animation(current_health):
	var current_anim = tree_animation.current_animation
	
	if ((current_health <= 0)):
		if (current_anim != "Muerto"):
			tree_animation.play("Muerto")
	else:
		if (current_health < TreeBehavior.MAX_HEALTH / 3):
			if (current_anim != "Sano3"):
				tree_animation.play("Sano3")
		else:
			if (current_health < TreeBehavior.MAX_HEALTH / 3 * 2):
				if (current_anim != "Sano2"):
					tree_animation.play("Sano2")
			else:
				if (current_anim != "Sano1"):
					tree_animation.play("Sano1")
