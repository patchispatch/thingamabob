extends Node
class_name RoadAnimationManager

@onready var road := $Road
@onready var road_animation := $Road/AnimationPlayer
@onready var steering_wheel_animation := $"Steering-wheel/AnimationPlayer"
@onready var rearview_animation := $"RearviewMirror/AnimationPlayer"

# Called when the node enters the scene tree for the first time.
func _ready():
	road_animation.play("Recto")
	steering_wheel_animation.play("Recto")
	rearview_animation.play("Normal")

func animate_left_turn() -> void:
	if (road_animation.current_animation == "Recto"):
		road_animation.play("Izquierda-input")
		rearview_animation.play("OjoIzquierda")
	
func animate_right_turn() -> void:
	if (road_animation.current_animation == "Recto"):
		road_animation.play("Derecha-input")
		rearview_animation.play("OjoDerecha")
	
func animate_success(direction: CurveType) -> void:
	if(direction.value == "left"):
		road_animation.play("Izquierda-exito")
		steering_wheel_animation.play("Izquierda")
		rearview_animation.play("Contento")
	else:
		road_animation.play("Derecha-exito")
		steering_wheel_animation.play("Derecha")
		rearview_animation.play("Contento")
	pass

func animate_crash(direction: CurveType) -> void:
	rearview_animation.play("Fail")
	if(direction.value == "left"):
		road_animation.play("Crash-izquierda")
		steering_wheel_animation.play("Izquierda")
	else:
		road_animation.play("Crash-derecha")
		steering_wheel_animation.play("Derecha")
	pass
	

func animate_bonsai_death() -> void:
	rearview_animation.play("Fail")

	
func animate_straighten_road() -> void:
	if (road_animation.current_animation != "Recto" && road_animation.current_animation != "Crash-izquierda" 
	&& road_animation.current_animation != "Crash-derecha"):
		road_animation.play("Recto")
		steering_wheel_animation.play("Recto")
		rearview_animation.play("Normal")
