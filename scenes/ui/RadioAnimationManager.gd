extends Node
class_name RadioAnimationManager

@onready var radio_marker := $RadioMarker
@onready var radio_marker_animation := $RadioMarker/AnimationPlayer
@onready var radio_button_up_animation := $RadioButtonUp/AnimationPlayer
@onready var radio_button_down_animation := $RadioButtonDown/AnimationPlayer
@onready var radio_reset_animation := $RadioReset/AnimationPlayer
@onready var bubble_animation := $Bubble/AnimationPlayer


func animate_button_up(is_pressed: bool) -> void:
	if (is_pressed && radio_button_up_animation.current_animation == "Default"):
		radio_button_up_animation.play("Press")
	else:
		if (!is_pressed && radio_button_up_animation.current_animation == "Press"):
			radio_button_up_animation.play("Default")

func animate_button_down(is_pressed: bool) -> void:
	if (is_pressed && radio_button_down_animation.current_animation == "Default"):
		radio_button_down_animation.play("Press")
	else:
		if (!is_pressed && radio_button_down_animation.current_animation == "Press"):
			radio_button_down_animation.play("Default")


func animate_radio_marker(current_freq: int) -> void:
	var marker_animation_to_play = "E" + str(current_freq+1)
	radio_marker_animation.play(marker_animation_to_play)

func animate_noise_bubble(freq_type: FreqType) -> void:
	match freq_type.value:
		FreqType.GOOD:
			bubble_animation.play("Musica")
		FreqType.NEAR_GOOD:
			bubble_animation.play("MusicaCerca")
		FreqType.BAD:
			bubble_animation.play("Rayos")
		FreqType.NEAR_BAD:
			bubble_animation.play("RayosCerca")
		FreqType.NOISE:
			bubble_animation.play("Vacio")

func animate_crash() -> void:
	bubble_animation.play("Vacio")

func animate_radio_reset() -> void:
	radio_reset_animation.play("Rayajo")
