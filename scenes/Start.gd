extends Node

@onready var start := $Start
@onready var game_connector := $GameConnector

var has_game_started: bool


func _ready():
	has_game_started = false


func _input(event):
	if event.is_action_pressed("GameStart") && not has_game_started:
		# Changes the scene
		remove_child(start)
		game_connector.start()
		has_game_started = true
