extends Enum
class_name TreeStatus

const GREAT = "great"
const GOOD = "good"
const BAD = "bad"
const DEAD = "dead"

static func great() -> TreeStatus:
    return TreeStatus.new(GREAT)


static func good() -> TreeStatus:
    return TreeStatus.new(GOOD)


static func bad() -> TreeStatus:
    return TreeStatus.new(BAD)


static func dead() -> TreeStatus:
    return TreeStatus.new(DEAD)