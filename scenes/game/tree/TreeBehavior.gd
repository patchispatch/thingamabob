extends Node
class_name TreeBehavior

const MAX_HEALTH = 20.0

var health: float

signal health_updated(health: float)


func _init():
	health = MAX_HEALTH


func _heal(increment: float) -> void:
	health = min(health + increment, MAX_HEALTH)


func _harm(decrement: float) -> void:
	health = max(health - decrement, 0)


func is_alive() -> bool:
	return health > 0


func get_status() -> TreeStatus:
	if health <= 0:
		return TreeStatus.dead()

	if health < MAX_HEALTH / 2:
		return TreeStatus.bad()
	
	if health == MAX_HEALTH:
		return TreeStatus.great()
	
	return TreeStatus.good()


func handle_plant_health(freq_type: FreqType, delta: float) -> void:
	match freq_type.value:
		FreqType.GOOD:
			_heal(TreeConsts.GOOD_HEAL_PER_SECOND * delta)
		FreqType.NEAR_GOOD:
			_heal(TreeConsts.NEAR_GOOD_HEAL_PER_SECOND * delta)
		FreqType.NOISE:
			_harm(TreeConsts.NOISE_HARM_PER_SECOND * delta)
		FreqType.NEAR_BAD:
			_harm(TreeConsts.NEAR_BAD_HARM_PER_SECOND * delta)
		FreqType.BAD:
			_harm(TreeConsts.BAD_HARM_PER_SECOND * delta)
	
	health_updated.emit(health)
