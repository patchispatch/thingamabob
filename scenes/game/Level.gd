extends Node
class_name Level

var radio: Dictionary
var road: Dictionary

func _init(road_info: Dictionary, radio_info: Dictionary):
	road = road_info
	radio = radio_info

static func from_level_data(level_data) -> Level:
	var formatted_road = {}
	var formatted_radio = {}
	var road_data = level_data["road"]
	var radio_data = level_data["radio"]

	for curve in road_data:
		match road_data[curve]:
			"left":
				formatted_road[curve] = CurveType.left()
			"right":
				formatted_road[curve] = CurveType.right()
			_:
				assert(false, "Wrong value in level data: [%i]: %s" % [curve, formatted_road[curve]])
	
	for radio_event in radio_data:
		match radio_data[radio_event]:
			"reset":
				formatted_radio[radio_event] = RadioEvent.reset()
			_:
				assert(false, "Wrong value in level data: [%i]: %s" % [radio_event, formatted_radio[radio_event]])

	return Level.new(formatted_road, formatted_radio)

func radio_at(second: int) -> RadioEvent:
	var value = radio.get("%d" % second)
	return value if value != null else RadioEvent.none()

func road_at(second: int) -> CurveType:
	var value = road.get("%d" % second)
	return value if value != null else CurveType.none()
