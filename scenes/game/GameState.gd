extends Node

@onready var progress_timer := $ProgressTimer
@onready var radio_behavior := $RadioBehavior
@onready var tree_behavior := $TreeBehavior
@onready var car_behavior := $CarBehavior

var level: Level
var _current_second: int
var _current_radio_event: RadioEvent
var _current_curve: CurveType
var _is_game_active: bool

signal radioprevious_is_pressed(is_pressed: bool)
signal radionext_is_pressed(is_pressed: bool)
signal road_begin_left_turn()
signal road_begin_right_turn()
signal game_start
signal game_over
signal bonsai_death_animation
signal victory


func _ready():
	# Connect signals
	car_behavior.car_crashes.connect(_handle_car_crash.unbind(1))
	progress_timer.timeout.connect(_handle_victory)

	level = Level.from_level_data(LevelLoader.load_level("demo"))


func start_game():
	_current_second = 0
	_current_radio_event = RadioEvent.none()
	_current_curve = CurveType.none()
	_is_game_active = true

	radio_behavior.init_radio()
	progress_timer.start()

	game_start.emit()


func _process(delta):
	if _is_game_active:
		# Handle level events
		if _has_passed_one_second():
			_handle_second_step_events()
		
		_handle_continuous_events(delta)


func _handle_second_step_events():
	# Make the car go straight again
	car_behavior.reset(_current_curve)

	# Advance to next second
	_current_second += 1
	_update_current_level_events()
	_handle_current_level_events()


func _handle_continuous_events(delta: float):
	# Update tree state
	var current_station_type: FreqType = radio_behavior.get_current_freq_type()
	tree_behavior.handle_plant_health(current_station_type, delta)

	# Check tree state
	if not tree_behavior.is_alive():
		bonsai_death_animation.emit()
		_set_game_over("bonsaideath")


func _has_passed_one_second() -> bool:
	return progress_timer.time_left as int < (progress_timer.wait_time as int - _current_second)


func _update_current_level_events() -> void:
	_current_radio_event = level.radio_at(_current_second)
	_current_curve = level.road_at(_current_second)


func _handle_current_level_events() -> void:
	if _current_radio_event.is_reset():
		radio_behavior.reset_radio()
	
	if _current_curve.is_left():
	# Trigger curve animation
		road_begin_left_turn.emit()
		
	elif _current_curve.is_right():
	# Trigger curve animation
		road_begin_right_turn.emit()
	

func _input(event):
	if _is_game_active:
		if event.is_action_pressed("CarLeft"):
			car_behavior.turn(CurveType.left(), _current_curve)
		if event.is_action_pressed("CarRight"):
			car_behavior.turn(CurveType.right(),_current_curve)

		if event.is_action_pressed("RadioNext"):
			radio_behavior.go_forward()
			radionext_is_pressed.emit(true)
		if event.is_action_released("RadioNext"):
			radionext_is_pressed.emit(false);
			
		if event.is_action_pressed("RadioPrevious"):
			radio_behavior.go_back()
			radioprevious_is_pressed.emit(true)
		if event.is_action_released("RadioPrevious"):
			radioprevious_is_pressed.emit(false);

func _handle_car_crash():
	_set_game_over("crash")


func _set_game_over(game_over_reason: String):
	_is_game_active = false
	progress_timer.stop()
	game_over.emit(game_over_reason)


func _handle_victory():
	_is_game_active = false
	victory.emit()
