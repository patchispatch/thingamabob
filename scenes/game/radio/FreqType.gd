extends Enum
class_name FreqType

const GOOD = "good"
const BAD = "bad"
const NEAR_GOOD = "near_good"
const NEAR_BAD = "near_bad"
const NOISE = "noise"

static func good() -> FreqType:
	return FreqType.new(GOOD)


static func bad() -> FreqType:
	return FreqType.new(BAD)


static func near_good() -> FreqType:
	return FreqType.new(NEAR_GOOD)


static func near_bad() -> FreqType:
	return FreqType.new(NEAR_BAD)


static func noise() -> FreqType:
	return FreqType.new(NOISE)


func is_good() -> bool:
	return value == GOOD


func is_bad() -> bool:
	return value == BAD


func is_near_good() -> bool:
	return value == NEAR_GOOD


func is_near_bad() -> bool:
	return value == NEAR_BAD


func is_noise() -> bool:
	return value == NOISE
