extends Node

@onready var good_station := $GoodStation
@onready var bad_station := $BadStation
@onready var noise := $Noise

const GOOD_RADIO_BUS_NAME = "good_radio"
const BAD_RADIO_BUS_NAME = "bad_radio"
const STATIC_RADIO_BUS_NAME = "static"

var good_radio_bus_id: int
var bad_radio_bus_id: int
var static_radio_bus_id: int


func _ready():
	good_radio_bus_id = AudioServer.get_bus_index(GOOD_RADIO_BUS_NAME)
	bad_radio_bus_id = AudioServer.get_bus_index(BAD_RADIO_BUS_NAME)
	static_radio_bus_id = AudioServer.get_bus_index(STATIC_RADIO_BUS_NAME)

	# Start with static
	_set_radio_bus_mute(true, true, false)

	# Bypass filters for radio stations by default
	_set_radio_bus_filters(good_radio_bus_id, false)
	_set_radio_bus_filters(bad_radio_bus_id, false)

	stop_stations_broadcast()


func start_stations_broadcast():
	good_station.start_broadcast()
	bad_station.start_broadcast()
	noise.start_broadcast()
	noise.play_next_program()


func stop_stations_broadcast():
	good_station.stop_broadcast()
	bad_station.stop_broadcast()
	noise.stop_broadcast()


func play_current_radio_station(freq_type: FreqType) -> void:
	match freq_type.value:
		FreqType.GOOD:
			_play_good_radio()
		FreqType.NEAR_GOOD:
			_play_good_radio(true)
		FreqType.BAD:
			_play_bad_radio()
		FreqType.NEAR_BAD:
			_play_bad_radio(true)
		FreqType.NOISE:
			_play_static_radio()
		

func advance_radio_stations_schedule():
	good_station.play_next_program()
	bad_station.play_next_program()


func _set_radio_bus_filters(bus_id: int, status: bool):
	AudioServer.set_bus_bypass_effects(bus_id, not status)


func _set_radio_bus_mute(good_radio: bool, bad_radio: bool, static_radio: bool):
	AudioServer.set_bus_mute(good_radio_bus_id, good_radio)
	AudioServer.set_bus_mute(bad_radio_bus_id, bad_radio)
	AudioServer.set_bus_mute(static_radio_bus_id, static_radio)


func _play_good_radio(filters=false):
	_set_radio_bus_mute(false, true, not filters)
	_set_radio_bus_filters(good_radio_bus_id, filters)


func _play_bad_radio(filters=false):
	_set_radio_bus_mute(true, false, not filters)
	_set_radio_bus_filters(bad_radio_bus_id, filters)


func _play_static_radio():
	_set_radio_bus_mute(true, true, false)
