extends Node


@onready var stream_player := $StreamPlayer

@export var schedule: Array
@export var target_bus: StringName

var _current_program: int


func _ready():
	_current_program = 0
	stream_player.bus = target_bus
	_play_program(_current_program)


func _play_program(program: int):
	var next_program := _get_program_stream(program)
	stream_player.stream = next_program
	stream_player.play()


func play_next_program():
	if _current_program + 1 < schedule.size():
		_current_program += 1
		_play_program(_current_program)


func _get_program_stream(program_index) -> AudioStream:
	if program_index < 0:
		assert(false, "Audio index out of bounds")

	return AudioLoader.load_audio(schedule[program_index])


func start_broadcast():
	stream_player.play()


func stop_broadcast():
	stream_player.stop()
