extends Enum
class_name RadioEvent

const RESET = "reset"
const NONE = "none"

static func reset() -> RadioEvent:
    return RadioEvent.new(RESET)

static func none() -> RadioEvent:
    return RadioEvent.new(NONE)   

func is_reset() -> bool:
    return value == RESET

func is_none() -> bool:
    return value == NONE
    