extends Node
class_name RadioBehavior

var radio_layout: RadioLayout
var current_freq: int

signal radio_updated(freq_type: FreqType)
signal radio_moved_manually
signal radio_current_freq(current_freq: int)
signal radio_at_limit
signal radio_is_reset


func init_radio() -> void:
	radio_layout = RadioLayout.default_rand()
	current_freq = radio_layout.get_rand_starting_position()


func reset_radio() -> void:
	init_radio()

	radio_is_reset.emit()
	_emit_radio_update()


func get_current_freq_type() -> FreqType:
	return radio_layout.get_freq_type(current_freq)


func go_forward() -> bool:
	"""
	Returns true if succesful, false if already at last freq
	"""
	if current_freq == radio_layout.freq_size - 1:
		radio_at_limit.emit()
		return false
	
	current_freq += 1

	_emit_radio_update()
	radio_moved_manually.emit()
	return true


func go_back() -> bool:
	"""
	Returns true if succesful, false if already at first freq
	"""
	if current_freq == 0:
		radio_at_limit.emit()
		return false
	
	current_freq -= 1

	_emit_radio_update()
	radio_moved_manually.emit()
	return true


func _emit_radio_update():
	radio_updated.emit(get_current_freq_type())
	radio_current_freq.emit(current_freq)
