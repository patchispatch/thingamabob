extends Node
class_name RadioLayout


# Properties
var freq_size: int
var good_station_freq: int
var bad_station_freq: int


# Make sure good_station_pos and bad_station_pos are in [0, freq_size)
func _init(good_fq: int = RadioConsts.DEFAULT_LEFT_STATION_POS, 
		   bad_fq: int = RadioConsts.DEFAULT_RIGHT_STATION_POS,
		   size: int = RadioConsts.DEFAULT_FREQ_SIZE) -> void:
	freq_size = size
	good_station_freq = good_fq
	bad_station_freq = bad_fq


func get_rand_starting_position() -> int:
	var neutral_freqs = _get_neutral_positions()
	return neutral_freqs[randi() % neutral_freqs.size()]


func get_freq_type(freq_index: int) -> FreqType:
	if freq_index == good_station_freq:
			return FreqType.good()

	if freq_index == bad_station_freq:
			return FreqType.bad()
	
	if _is_near_good(freq_index):
		return FreqType.near_good()

	if _is_near_bad(freq_index):
			return FreqType.near_bad()
	
	return FreqType.noise()
		

func _get_adjacent_values(value: int) -> Array:
	var adjacent = []
	if 0 < value:
		adjacent.push_back(value -1)
	if value < freq_size - 1:
		adjacent.push_back(value + 1)
	
	return adjacent


func _is_near_good(val: int) -> bool:
	if val - 1 == good_station_freq or val + 1 == good_station_freq:
		return true
	
	return false


func _is_near_bad(val: int) -> bool:
	if val - 1 == bad_station_freq or val + 1 == bad_station_freq:
		return true
	
	return false


func _get_neutral_positions() -> Array:
	return range(freq_size) \
		.filter(func(x): return \
			!_is_near_bad(x) \
			and !_is_near_good(x) \
			and x != good_station_freq \
			and x != bad_station_freq)


static func default_rand() -> RadioLayout:
	return RadioLayout.new() if randi() % 2 \
		else RadioLayout.new(RadioConsts.DEFAULT_RIGHT_STATION_POS, RadioConsts.DEFAULT_LEFT_STATION_POS)
