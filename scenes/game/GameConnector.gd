extends Node

@onready var game_state := $GameState
@onready var radio_behavior := $GameState/RadioBehavior
@onready var tree_behavior := $GameState/TreeBehavior
@onready var car_behavior := $GameState/CarBehavior
@onready var radio_sound_manager := $RadioSoundManager
@onready var sfx_manager := $SFXManager
@onready var view := $View
@onready var radio_animation_manager := $View/RadioAnimationManager
@onready var road_animation_manager := $View/RoadAnimationManager
@onready var tree_animation_manager := $View/TreeAnimationManager
@onready var transition := $SceneTransition

# Start the game
func start():
	game_state.start_game()
	radio_sound_manager.start_stations_broadcast()


func _ready():
	_connect_radio_behavior_with_sound()
	
	_connect_radio_behavior_with_sfx()
	_connect_car_behavior_with_sfx()

	_connect_radio_behavior_and_inputs_with_animation()
	_connect_car_behavior_with_animation()

	_connect_tree_behavior_with_animation()

	_connect_game_over_with_everything()
	_connect_victory_with_everything()


func _connect_radio_behavior_with_sound():
	radio_behavior.radio_updated.connect(radio_sound_manager.play_current_radio_station)
	radio_behavior.radio_is_reset.connect(radio_sound_manager.advance_radio_stations_schedule)


func _connect_radio_behavior_with_sfx():
	radio_behavior.radio_moved_manually.connect(sfx_manager.play_radio_slider_move)
	radio_behavior.radio_at_limit.connect(sfx_manager.play_radio_slider_cant_move)


func _connect_car_behavior_with_sfx():
	car_behavior.car_turns.connect(sfx_manager.play_car_turn_successful.unbind(1))
	car_behavior.car_crashes.connect(sfx_manager.play_car_turn_failure.unbind(1))


func _connect_radio_behavior_and_inputs_with_animation():
	radio_behavior.radio_current_freq.connect(radio_animation_manager.animate_radio_marker)
	game_state.radioprevious_is_pressed.connect(radio_animation_manager.animate_button_down)
	game_state.radionext_is_pressed.connect(radio_animation_manager.animate_button_up)
	radio_behavior.radio_is_reset.connect(radio_animation_manager.animate_radio_reset)
	radio_behavior.radio_updated.connect(radio_animation_manager.animate_noise_bubble)
	car_behavior.car_crashes.connect(radio_animation_manager.animate_crash.unbind(1))


func _connect_tree_behavior_with_animation():
	tree_behavior.health_updated.connect(tree_animation_manager.play_current_animation)
	game_state.bonsai_death_animation.connect(road_animation_manager.animate_bonsai_death)


func _connect_car_behavior_with_animation():
	game_state.road_begin_left_turn.connect(road_animation_manager.animate_left_turn)
	game_state.road_begin_right_turn.connect(road_animation_manager.animate_right_turn)
	car_behavior.car_turns.connect(road_animation_manager.animate_success)
	car_behavior.car_crashes.connect(road_animation_manager.animate_crash)
	car_behavior.car_goes_straight.connect(road_animation_manager.animate_straighten_road)


func _connect_game_over_with_everything():
	# Sound
	game_state.game_over.connect(radio_sound_manager.stop_stations_broadcast.unbind(1))

	# Transition
	# TODO: connect with car crash animation or tree death finish
	game_state.game_over.connect(transition.to_game_over)


func _connect_victory_with_everything():
	# Sound
	game_state.victory.connect(radio_sound_manager.stop_stations_broadcast)

	# Transition
	# TODO: connect with victory animation finish
	game_state.victory.connect(transition.to_victory_scene)
