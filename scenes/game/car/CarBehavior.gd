extends Node
class_name CarBehavior

var _turned: bool

signal car_goes_straight()
signal car_turns(direction: CurveType)
signal car_crashes(direction: CurveType)

func _init():
	_turned = false


func turn(direction: CurveType, road_status: CurveType) -> void:
	# If current direction is none/forward
	if not _turned:
		_turned = true

		var result = road_status.value == direction.value
		if result:
			car_turns.emit(direction)
		else:
			car_crashes.emit(direction)


func has_turned() -> bool:
	return _turned

	
func reset(road_status) -> void:
	if not road_status.is_none() and not _turned:
		car_crashes.emit(road_status)
	else:
		if road_status.is_none():
			car_goes_straight.emit()

	_turned = false
