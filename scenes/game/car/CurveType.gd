extends Enum
class_name CurveType

const LEFT = "left"
const RIGHT = "right"
const NONE = "none"

static func left() -> CurveType:
    return CurveType.new(LEFT)

static func right() -> CurveType:
    return CurveType.new(RIGHT)

static func none() -> CurveType:
    return CurveType.new(NONE)

func is_left() -> bool:
    return value == LEFT

func is_right() -> bool:
    return value == RIGHT

func is_none() -> bool:
    return value == NONE

