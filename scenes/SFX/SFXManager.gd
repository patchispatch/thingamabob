extends Node
class_name SFXManager

# Radio
@onready var radio_slider_move := $RadioSliderMove
@onready var radio_slider_cant_move := $RadioSliderCantMove

# Car
@onready var car_turn_successful := $CarTurnSuccessful
@onready var car_turn_failure := $CarTurnFailure


func play_radio_slider_move():
    radio_slider_move.play()


func play_radio_slider_cant_move():
    radio_slider_cant_move.play()


func play_car_turn_successful():
    car_turn_successful.play()


func play_car_turn_failure():
    car_turn_failure.play()

