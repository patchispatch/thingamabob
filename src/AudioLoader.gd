extends Node
class_name AudioLoader

const AUDIO_PATH_TEMPLATE = "res://assets/audio/%s.ogg"


static func _get_audio_path(audio_id) -> String:
	return AUDIO_PATH_TEMPLATE % audio_id


static func load_audio(audio_id: String) -> AudioStream:
	var audio_path := _get_audio_path(audio_id)
	return load(audio_path)
