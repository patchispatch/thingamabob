extends Node
class_name LevelLoader

const LEVEL_PATH = "res://assets/levels/%s.json"

static func _get_level_path(level_id) -> String:
    return LEVEL_PATH % level_id

static func _get_json_data_from_file(file_path: String):
    assert(FileAccess.file_exists(file_path), "File path does not exist")

    var file := FileAccess.open(file_path, FileAccess.READ)
    var json_file := JSON.new()
    var parse_result := json_file.parse(file.get_as_text())

    if parse_result != OK:
        assert(false, "Error parsing file %s" % file_path)
        
    return json_file.get_data()

static func load_level(level_id: String):
    """
    Reads a level from a file and returns a dictionary containing road and radio details
    """
    var level_path := _get_level_path(level_id)

    return _get_json_data_from_file(level_path)

