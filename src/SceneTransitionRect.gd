extends Control

@export_file("*.tscn") var game_over_scene: String
@export_file("*.tscn") var victory_scene: String

@onready var animation_player := $AnimationPlayer
@onready var black_rect := $BlackRect
@onready var white_rect := $WhiteRect


func to_game_over(game_over_reason: String) -> void:
	# TODO: remove
	await _wait()
	# Plays the Fade animation and wait until it finishes
	black_rect.visible = true
	animation_player.play("fade")
	await animation_player.animation_finished
	# Changes the scene
	Global.game_over_reason = game_over_reason
	get_tree().change_scene_to_file(game_over_scene)


func to_victory_scene() -> void:
	# Plays the Fade animation and wait until it finishes
	white_rect.visible = true
	animation_player.play("fade_white")
	await animation_player.animation_finished
	# Changes the scene
	get_tree().change_scene_to_file(victory_scene)


# TODO: remove
func _wait():
	var t = Timer.new()
	t.set_wait_time(3)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	await t.timeout
