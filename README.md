# Bonsai FM

Play it on [itch.io](https://cqto.itch.io/bonsai-fm)!

**Bonsai FM** is a bonsai-packed, radio-powered, 20-second musical trip made for the [20 SECOND GAME JAM 2023](https://itch.io/jam/20-second-game-jam-2023). 

## Screenshots

- TODO

## Team

- cqto [(itch page)](https://cqto.itch.io)
- Pabluwu [(itch page)](https://pabluwu707.itch.io/)
